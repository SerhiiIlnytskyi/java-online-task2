package com.epam.courses.model;

/**
 * The Guest entity.
 */
public class Guest {

    private String name;

    private boolean isHeard;

    /**
     * Gets guest name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets guest name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns if the rumor is heard.
     *
     * @return the boolean
     */
    public boolean isHeard() {
        return isHeard;
    }

    /**
     * Sets heard rumor.
     *
     * @param heard the heard
     */
    public void setHeard(boolean heard) {
        isHeard = heard;
    }
}
