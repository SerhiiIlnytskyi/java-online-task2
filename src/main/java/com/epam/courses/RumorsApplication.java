package com.epam.courses;

import com.epam.courses.model.Guest;
import com.epam.courses.model.Party;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The Rumors application main class
 */
public class RumorsApplication
{

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        System.out.println("Please insert the number of guests without Alice ");
        System.out.print("The number must be more than 2: ");

        Scanner scanner = new Scanner(System.in);

        int guestsCount = scanner.nextInt();

        List<Guest> guests = new ArrayList<>();

        for (int i = 0; i < guestsCount; i++) {
            guests.add(new Guest());
        }

        Party party = new Party(guests);

        double probabilityAll = party.probabilityFullSpreadRumor();

        int generalOfHeared = 0;
        for (int i = 0; i < 1000; i++) {
            party.partyInit();
            generalOfHeared += party.numberOfPeopleHeardRumor();
        }

        double expectedWhoheared = (double) generalOfHeared/1000;


        System.out.printf("Expected who heared = %.3f People; Probability All = %.5f%%", expectedWhoheared, probabilityAll);
    }

    
}
